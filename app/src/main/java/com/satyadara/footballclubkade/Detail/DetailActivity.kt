package com.satyadara.footballclubkade.Detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import com.satyadara.fcapp.FootballClub
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find
import org.jetbrains.anko.image
import org.jetbrains.anko.setContentView


class DetailActivity : AppCompatActivity() {
    private lateinit var mLogo: ImageView
    private lateinit var mDescription: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DetailActivityUI().setContentView(this)

        var footballClub: FootballClub = intent.getParcelableExtra("fc")

        mLogo = findViewById(DetailActivityUI.ivLogo)
        mDescription = findViewById(DetailActivityUI.tvDescription)

        Picasso.get()
            .load(footballClub.logo)
            .resize(DetailActivityUI.logoDipSize, DetailActivityUI.logoDipSize)
            .into(mLogo)
        mDescription.text = "Ini adalah deskripsi dari klub ${footballClub.name}"
    }
}