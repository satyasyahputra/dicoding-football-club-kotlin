package com.satyadara.footballclubkade.Main

import android.support.constraint.ConstraintSet.PARENT_ID
import android.support.v7.widget.LinearLayoutManager
import com.satyadara.footballclubkade.MainActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView

class MainActivityUI : AnkoComponent<MainActivity> {
    companion object {
        val rvFootballClub = 10101022
    }

    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        constraintLayout {
            padding = dip(16)
            lparams(width = matchParent, height = matchParent)
            recyclerView {
                id = rvFootballClub
                layoutManager = LinearLayoutManager(context)
            }.lparams(matchParent, matchParent) {
                topToTop = PARENT_ID
                leftToLeft = PARENT_ID
                rightToRight = PARENT_ID
                leftToLeft = PARENT_ID
            }
        }
    }
}